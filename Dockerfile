#docker build -t [microservice-name]-[version] -f Dockerfile .
FROM openjdk:8-jre
# Microservicio port
EXPOSE 8080

ADD build/libs/ms-gestionVersionApp-neg-1.0.0.jar /app/ms-gestionVersionApp-neg-1.0.0.jar

# Fix zona horaria de Chile
RUN apt-get update && apt-get install -y tzdata
ENV TZ America/Santiago

WORKDIR /app
CMD if [ -n "${DNS1}" ]; then echo "nameserver ${DNS1}\n" > /etc/resolv.conf; fi \
    && if [ -n "${DNS2}" ]; then echo "nameserver ${DNS2}\n" >> /etc/resolv.conf; fi \
	&& java -Xms${HEAP_SIZE}m -Xmx${HEAP_SIZE}m -Djava.security.egd=file:/dev/./urandom ${JAVA_PROPS} -jar ms-gestionVersionApp-neg-1.0.0.jar