package acceptance.steps;

import acceptance.base.TestAcceptanceBase;
import acceptance.util.Login;
import cl.bci.gestionversionapp.model.*;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Entonces;
import org.springframework.http.*;

import java.net.URI;

import static org.assertj.core.api.Assertions.assertThat;

public class AdministrarVersionSteps extends TestAcceptanceBase{

    private final String URL_LISTADO_VERSIONES = URI_MS + "/Transversales/Utilitarios/ms-gestionVersionApp-neg-V1.0/solicitarVersiones";
    private final String URL_INSERT_VERSIONES = URI_MS + "/Transversales/Utilitarios/ms-gestionVersionApp-neg-V1.0/configurarVersion";
    private ResponseEntity<VersionesAppResponseDto> lastResponseListado;
    private ResponseEntity<VersionDto> lastResponseInsertUpdate;

    @Cuando("^solicito el listado de versiones$")
    public void solicito_el_listado_de_versiones() throws Throwable {
        HttpHeaders headers = getBaseHeaders();
        headers.add("Authorization", "bearer " + Login.getLoggedTokenClientCredentials());

        RequestEntity<String> requestEntity = new RequestEntity<>(null, headers, HttpMethod.GET, URI.create(URL_LISTADO_VERSIONES));
        lastResponseListado = restTemplate.exchange(requestEntity,
                VersionesAppResponseDto.class);
    }

    @Entonces("^recibo la lista que esta registrada$")
    public void recibo_la_lista_que_esta_registrada() throws Throwable {
        assertThat(lastResponseListado.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(lastResponseListado.getBody()).isNotNull();
    }

    @Cuando("^inserto la plataforma \"([^\"]*)\", version \"([^\"]*)\", tipo accion \"([^\"]*)\" y mensaje \"([^\"]*)\"$")
    public void inserto_la_plataforma_version_tipo_accion_y_mensaje(String plataforma, String version, String tipoAccion, String mensaje) throws Throwable {
        HttpHeaders headers = getBaseHeaders();
        headers.add("Authorization", "bearer " + Login.getLoggedTokenClientCredentials());

        AccionDto accionDto = AccionDto.builder().tipoAccion(tipoAccion).mensaje(mensaje).build();
        lastResponseInsertUpdate = restTemplate.postForEntity(URL_INSERT_VERSIONES,
                new HttpEntity<>(VersionDto.builder().plataforma(plataforma).version(version).accion(accionDto).build(), headers),
                VersionDto.class);
    }

    @Entonces("^recibo un estado (\\d+)$")
    public void recibo_un_estado(int arg1) throws Throwable {
        if ( arg1 == 200) {
            assertThat(lastResponseInsertUpdate.getStatusCode()).isEqualTo(HttpStatus.OK);
            assertThat(lastResponseInsertUpdate.getBody()).isNotNull();
        }
        else{
            assertThat(lastResponseInsertUpdate.getStatusCodeValue()).isEqualTo(arg1);
        }
    }

}
