package acceptance.steps;

import acceptance.base.TestAcceptanceBase;
import acceptance.util.Login;
import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cucumber.api.java.Before;
import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import org.springframework.http.*;

import static org.assertj.core.api.Assertions.assertThat;

public class ValidarVersionSteps extends TestAcceptanceBase{

    private final String URL_VALIDAR = URI_MS + "/Transversales/Utilitarios/ms-gestionVersionApp-neg-V1.0/validarVersionApp";
    private ResponseEntity<ValidarVersionAppResponseDto> lastResponse;

    @Dado("^registrada la plataforma \"([^\"]*)\", version \"([^\"]*)\" y tipoAccion \"([^\"]*)\"$")
    public void registrada_la_plataforma_version_y_tipoAccion(String plataforma, String version, String tipoAccion) throws Throwable {
        new Login().un_token_publico_para("sistemaUpdateApp");
        new AdministrarVersionSteps().inserto_la_plataforma_version_tipo_accion_y_mensaje(plataforma,version,tipoAccion, "");
    }

    @Cuando("^solicito verificar la aplicacion para \"([^\"]*)\" y version \"([^\"]*)\"$")
    public void solicito_verificar_la_aplicacion_para_y_version(String plataforma, String version) throws Throwable {
        HttpHeaders headers = getBaseHeaders();
        headers.add("Authorization", "bearer " + Login.getLoggedTokenClientCredentials());

        lastResponse = restTemplate.postForEntity(URL_VALIDAR,
                new HttpEntity<>(ValidarVersionAppRequestDto.builder().plataforma(plataforma).version(version).build(), headers),
                ValidarVersionAppResponseDto.class);
    }

    @Entonces("^recibo el tipo de accion \"([^\"]*)\"$")
    public void recibo_el_tipo_de_accion(String tipoAccion) throws Throwable {
        assertThat(lastResponse.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(lastResponse.getBody().getTipoAccion()).isEqualToIgnoringCase(tipoAccion);
    }


}
