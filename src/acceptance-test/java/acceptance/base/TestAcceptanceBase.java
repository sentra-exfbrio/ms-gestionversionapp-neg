package acceptance.base;

import acceptance.util.Login;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.client.RestTemplate;
import acceptance.util.Utilitarios;


@ContextConfiguration
public class TestAcceptanceBase {
	
	protected String APPLICATION_NAME = "ms-gestionversionapp-neg";
	protected String URI_ZUUL = Utilitarios.obtenerVariableEntorno("ZUUL_SERVER_URI");
	protected String MS_VERSION = Utilitarios.obtenerVariableEntorno("MS_VERSION");
	//protected String URI_MS = "http://" + URI_ZUUL + "/" + APPLICATION_NAME + "_" + MS_VERSION;
	protected String URI_MS = "http://localhost:8022/" + APPLICATION_NAME;

	protected RestTemplate restTemplate = new RestTemplate();

	private final String ACCEPTANCE_TEST = APPLICATION_NAME + "_acceptanceTest";

	protected HttpHeaders getBaseHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Tracking-Id", ACCEPTANCE_TEST);
		headers.add("Application-Id", ACCEPTANCE_TEST);
		headers.add("Channel", ACCEPTANCE_TEST);
		headers.add("Reference-Service", ACCEPTANCE_TEST);
		headers.add("Reference-Operation", ACCEPTANCE_TEST);
		headers.add("Origin-addr", ACCEPTANCE_TEST);
		return headers;
	}
}
