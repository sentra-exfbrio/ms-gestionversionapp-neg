package acceptance.util;

import cucumber.api.java.es.Dado;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import com.fasterxml.jackson.annotation.JsonProperty;

import acceptance.base.TestAcceptanceBase;

public class Login extends TestAcceptanceBase{
	
	private RestTemplate restTemplate = new RestTemplate();
	
	private final String BASIC_AUTH = "Basic YXBwLW1vdmlsLXBlcnNvbmFzOnNlY3JldGlzaW1v";
	private final String LOGIN_URI = "api-login/oauth/token";
	//private final String LOGIN_URL = (URI_ZUUL+LOGIN_URI).trim();
	private final String LOGIN_URL = "http://localhost:8080/" + LOGIN_URI;
	private static String loggedInToken;
	private static String loggedTokenClientCredentials;

	public String getToken(String rut, String password) {
		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", BASIC_AUTH);
		headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
		requestBody.add("username", rut);
		requestBody.add("password", password);
		requestBody.add("grant_type", "password");
		HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<>(requestBody, headers);
		
		return this.restTemplate.exchange(LOGIN_URL, HttpMethod.POST, formEntity, AuthenticationResponse.class)
				.getBody().getAccessToken();
	}
	
	public String getLoggedInToken() {
		return loggedInToken;
	}
	
	public void setLoggedInToken(String tk) {
		loggedInToken = tk;
	}

	static class AuthenticationResponse {
		@JsonProperty(value = "access_token")
		private String accessToken;

		public String getAccessToken() {
			return accessToken;
		}
	}

	@Dado("^un token publico para \"([^\"]*)\"$")
	public void un_token_publico_para(String tipoToken) throws Throwable {
		HttpHeaders headers = getBaseHeaders();
		if ("updateApp".equalsIgnoreCase(tipoToken)) {
			headers.add("Authorization", "Basic YXBwLXB1YmxpY1VwZGF0ZUFwcDpjWEptN05qZGdUeDZyWFpL");
		}
		else if ("sistemaUpdateApp".equalsIgnoreCase(tipoToken)){
			headers.add("Authorization", "Basic YXBwLXNpc3RlbWFVcGRhdGVBcHA6SzJXVlNxTjZSdkNkWDJqeQ==");
		}
		headers.add("Content-Type", MediaType.APPLICATION_FORM_URLENCODED_VALUE);
		headers.add("Accept", MediaType.APPLICATION_JSON_VALUE);

		MultiValueMap<String, String> requestBody = new LinkedMultiValueMap<>();
		requestBody.add("grant_type", "client_credentials");
		HttpEntity<MultiValueMap<String, String>> formEntity = new HttpEntity<>(requestBody, headers);

		this.loggedTokenClientCredentials = this.restTemplate.exchange(LOGIN_URL, HttpMethod.POST, formEntity, AuthenticationResponse.class)
				.getBody().getAccessToken();
	}

	public static String getLoggedTokenClientCredentials() {
		return loggedTokenClientCredentials;
	}
}
