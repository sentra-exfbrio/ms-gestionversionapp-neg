#language: es
Característica: Verificar la version de la App para permitir acceso.
  Como cliente Bco
  Quiero abrir mi app
  Para poder acceder a mi cuenta y servicios del banco

  Esquema del escenario: Obtener accion a realizar segun la version de la app
    Dado registrada la plataforma "<SO>", version "<version>" y tipoAccion "<tipoAccion>"
    Y un token publico para "updateApp"
    Cuando solicito verificar la aplicacion para "<SO>" y version "<version>"
    Entonces recibo el tipo de accion "<tipoAccion>"
    Ejemplos:
      |   SO    | version | tipoAccion |
      | Android |  1.0.0  |  CADUCADA  |
      | ANDROID |  1.7.0  | ACTUALIZAR |
      | AndroiD |  2.0.0  |  VIGENTE   |
      | AndrOiD |  0.0.0  |  VIGENTE   |
      |   ios   |  1.0.0  |  CADUCADA  |
      |   iOS   |  1.7.0  | ACTUALIZAR |
      |   IOS   |  2.0.0  |  VIGENTE   |
      |   IoS   |  0.0.0  |  VIGENTE   |
      |   IOS   |  0.0.1  | MANTENCION |