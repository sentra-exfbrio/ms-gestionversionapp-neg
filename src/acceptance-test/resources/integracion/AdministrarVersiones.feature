#language: es
Característica: Administrar las versiones para solicitar la actualizacion al usuario en caso de ser necesario
  Como personal de operaciones de bci
  Quiero administrar las versiones registradas de la app mobil soñada
  Para poder informar a los clientes cuando deben actualizar su app

  Escenario: Listar versiones
    Dado un token publico para "sistemaUpdateApp"
    Cuando solicito el listado de versiones
    Entonces recibo la lista que esta registrada


  Esquema del escenario: Insertar o actualizar versiones
    Dado un token publico para "sistemaUpdateApp"
    Cuando inserto la plataforma "<plataforma>", version "<version>", tipo accion "<tipoAccion>" y mensaje "<mensaje>"
    Entonces recibo un estado <http>
    Ejemplos:
      | plataforma | version | tipoAccion |  mensaje  | http |
      | Android    | re7.0.0 |  VIGENTE   | v         | 460  |
      | Android    |  0.1.0  |  CADUCADA  | caducada  | 200  |
      | Android    |  0.1.0  | MANTEncION | mantenimiento  | 200  |
      | aioese     |  1.7.0  | ACTUALIZAR | oe actualiza   | 460  |
      | AndroiD    |  1.7.0  | EXPLOSION  | de galaxias    | 460  |