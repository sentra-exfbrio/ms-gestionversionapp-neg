package cl.bci.gestionversionapp.service;

import cl.bci.gestionversionapp.model.AccionDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cl.bci.gestionversionapp.model.VersionDto;
import cl.bci.gestionversionapp.repository.AccionesRepository;
import cl.bci.gestionversionapp.repository.VersionesRepository;
import cl.bci.gestionversionapp.repository.entity.AccionesEntity;
import cl.bci.gestionversionapp.repository.entity.VersionesEntity;
import cl.bci.gestionversionapp.util.VersionesUtil;
import cl.bci.mscore.exception.ErrorNegocioException;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
public class GestionVersionAppServiceImplTest {

    public String[] versiones;

    @Mock
    private DozerBeanMapper dozerBeanMapper;

    @Mock
    private VersionesUtil versionesUtil;

    @Mock
    private VersionesRepository versionesRepository;

    @Mock
    private AccionesRepository accionesRepository;

    @InjectMocks
    private GestionVersionAppServiceImpl gestionVersionAppService;

    @Before
    public void setUp() throws Exception {

        List<VersionesEntity> responseRepo = new ArrayList<>();
        AccionesEntity accion = AccionesEntity.builder().tipoAccion("ACTUALIZAR").mensaje("mensaje ACTUALIZAR").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.1.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje ").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.1").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("ACTUALIZAR").mensaje("mensaje ACTUALIZAR").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("MANTENCION").mensaje("mensaje MANTENCION").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.3.4").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje ").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.2.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje VIGENTE").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("MANTENCION").mensaje("mensaje MANTENCION").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("2.0.1").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("mensaje CADUCADA").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("2.2.1").accionesEntity(accion).build());
        when(versionesRepository.findByPlataforma("IOS")).thenReturn(responseRepo);

        versiones = responseRepo.stream().map(e -> e.getVersion()).toArray(String[]::new);

        List<String> plataformasPermitidas = new ArrayList<>();
        plataformasPermitidas.add("IOS");
        plataformasPermitidas.add("Android");
        ReflectionTestUtils.setField(gestionVersionAppService, "plataformasPermitidas", plataformasPermitidas);
        List<String> tiposAcciones = new ArrayList<>();
        tiposAcciones.add("VIGENTE");
        tiposAcciones.add("ACTUALIZAR");
        tiposAcciones.add("MANTENCION");
        tiposAcciones.add("CADUCADA");
        ReflectionTestUtils.setField(gestionVersionAppService, "tiposAcciones", tiposAcciones);

        String regexVersion ="^(\\d{0,2})\\.(\\d{0,2})\\.(\\d{0,2})$";
        ReflectionTestUtils.setField(gestionVersionAppService, "pattern", Pattern.compile(regexVersion));
        ReflectionTestUtils.setField(gestionVersionAppService, "tipoAccionDefault", "VIGENTE");
        ReflectionTestUtils.setField(gestionVersionAppService, "mensajeDefault", "mensajeDefault");
        ReflectionTestUtils.setField(gestionVersionAppService, "largoMensaje", 30);
    }

    @Test
    public void validarVersionApp_debeRetornarVigente_cuandoEncuentraVersionPorPlataforma() {
        ValidarVersionAppRequestDto request = new ValidarVersionAppRequestDto();
        request.setPlataforma("IOS");
        request.setVersion("1.0.0");

        AccionesEntity accionesEntity = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("Vigente mensaje").build();
        VersionesEntity responseRepo = VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accionesEntity).build();
        when(versionesRepository.findByPlataformaAndVersion(request.getPlataforma(),request.getVersion())).thenReturn(responseRepo);

        when(dozerBeanMapper.map(anyObject(), eq(ValidarVersionAppResponseDto.class))).thenReturn(ValidarVersionAppResponseDto.builder().tipoAccion("VIGENTE").mensaje("Vigente mensaje").version("1.0.0").build());
        ValidarVersionAppResponseDto response = gestionVersionAppService.validarVersionApp(request);
        assertThat(response.getTipoAccion()).isEqualTo("VIGENTE");
    }

    @Test
    public void validarVersionApp_debeRetornarMantencion_cuandoNoEncuentraVersionYBuscaInferior() {
        ValidarVersionAppRequestDto request = new ValidarVersionAppRequestDto();
        request.setPlataforma("IOS");
        request.setVersion("1.3.5");

        when(versionesRepository.findByPlataformaAndVersion(request.getPlataforma(),request.getVersion())).thenReturn(null);
        when(versionesUtil.obtenerVersionAnterior(request.getVersion(), versiones)).thenReturn("1.3.4");
        when(dozerBeanMapper.map(anyObject(), eq(ValidarVersionAppResponseDto.class))).thenReturn(ValidarVersionAppResponseDto.builder().tipoAccion("MANTENCION").mensaje("Vigente mensaje").version("1.0.0").build());
        ValidarVersionAppResponseDto response = gestionVersionAppService.validarVersionApp(request);
        assertThat(response.getTipoAccion()).isEqualTo("MANTENCION");
    }

    @Test
    public void validarVersionApp_debeRetornarVigente_cuandoNoEncuentraVersion() {
        ValidarVersionAppRequestDto request = new ValidarVersionAppRequestDto();
        request.setPlataforma("IOS");
        request.setVersion("1.3.5");

        when(versionesRepository.findByPlataformaAndVersion(request.getPlataforma(),request.getVersion())).thenReturn(null);
        when(versionesUtil.obtenerVersionAnterior(request.getVersion(), versiones)).thenReturn("");
        ValidarVersionAppResponseDto response = gestionVersionAppService.validarVersionApp(request);
        assertThat(response.getTipoAccion()).isEqualTo("VIGENTE");
    }

    @Test
    public void solicitarVersiones_debeRetornarVersiones() {
        VersionesEntity[] versionesEnt = new VersionesEntity[3];
        AccionesEntity accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje ").build();
        VersionesEntity versiones = VersionesEntity.builder().plataforma("IOS").version("1.2.2").accionesEntity(accion).build();
        versionesEnt[0]= versiones;
        versionesEnt[1]= versiones;
        versionesEnt[2]= versiones;

        Iterable<VersionesEntity> iterable = Arrays.asList(versionesEnt);
        when(versionesRepository.findAll()).thenReturn(iterable);
        List<VersionDto> retorno = gestionVersionAppService.solicitarVersiones();
        assertThat(retorno).isNotEmpty();
    }


    @Test
    public void ingresarVersion_debeRetornar_siEncuentraVersionActualizar() {

        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().version("1.1.1").plataforma("IOS").accion(accionDto).build();

        AccionesEntity accionesEntity = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("Vigente mensaje").build();
        VersionesEntity responseRepo = VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accionesEntity).build();
        when(versionesRepository.findByPlataformaAndVersion(version.getPlataforma(),version.getVersion())).thenReturn(responseRepo);
        when(versionesRepository.save(responseRepo)).thenReturn(responseRepo);
        when(dozerBeanMapper.map(anyObject(), eq(VersionDto.class))).thenReturn(version);


        VersionDto versionDto = gestionVersionAppService.ingresarVersion(version);
        assertThat(versionDto.getPlataforma()).isEqualTo("IOS");
    }


    @Test
    public void ingresarVersion_debeRetornar_siEncuentraVersionActualizarSinAccion() {

        AccionDto accionDto = AccionDto.builder().tipoAccion("MANTENCION").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().version("1.1.1").plataforma("IOS").accion(accionDto).build();

        AccionesEntity accionesEntity = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("Vigente mensaje").build();
        VersionesEntity responseRepo = VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accionesEntity).build();
        when(versionesRepository.findByPlataformaAndVersion(version.getPlataforma(),version.getVersion())).thenReturn(responseRepo);
        when(versionesRepository.save(responseRepo)).thenReturn(responseRepo);
        when(dozerBeanMapper.map(anyObject(), eq(VersionDto.class))).thenReturn(version);


        VersionDto versionDto = gestionVersionAppService.ingresarVersion(version);
        assertThat(versionDto.getPlataforma()).isEqualTo("IOS");
    }

    @Test
    public void ingresarVersion_debeRetornar_siIngresaVersion() {

        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().version("1.1.1").plataforma("IOS").accion(accionDto).build();

        AccionesEntity accionesEntity = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("Vigente mensaje").build();
        VersionesEntity responseRepo = VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accionesEntity).build();
        when(versionesRepository.findByPlataformaAndVersion(version.getPlataforma(),version.getVersion())).thenReturn(null);
        when(versionesRepository.save(responseRepo)).thenReturn(responseRepo);
        when(dozerBeanMapper.map(anyObject(), eq(VersionDto.class))).thenReturn(version);


        VersionDto versionDto = gestionVersionAppService.ingresarVersion(version);
        assertThat(versionDto.getPlataforma()).isEqualTo("IOS");
    }


    @Test
    public void ingresarVersion_debeRetornar_sitipoAccionNoNull() {

        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().version("1.1.1").plataforma("IOS").accion(accionDto).build();

        AccionesEntity accionesEntity = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("Vigente mensaje").build();
        VersionesEntity responseRepo = VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accionesEntity).build();
        when(versionesRepository.findByPlataformaAndVersion(version.getPlataforma(),version.getVersion())).thenReturn(null);
        AccionesEntity accionesEnt = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("mensaje").build();
        when(accionesRepository.findByTipoAccion(accionesEntity.getTipoAccion())).thenReturn(accionesEnt);
        when(versionesRepository.save(responseRepo)).thenReturn(responseRepo);
        when(dozerBeanMapper.map(anyObject(), eq(VersionDto.class))).thenReturn(version);


        VersionDto versionDto = gestionVersionAppService.ingresarVersion(version);
        assertThat(versionDto.getPlataforma()).isEqualTo("IOS");
    }


    @Test
    public void ingresarVersion_debeRetornarException_cuandoVersionNull() {
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(null)).isInstanceOf(ErrorNegocioException.class);

    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoVersionSinPlataforma() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().version("1.1.1").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);

    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoVersionPlataformaNOValida() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("NOVALIDA").version("1.1.1").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoVersionNoIngresada() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoVersionNoValida() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("a.s,s").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }


    @Test
    public void ingresarVersion_debeRetornarException_cuandoAccionEsNull() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("1.1.1").accion(null).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoTipoAccionNull() {
        AccionDto accionDto = AccionDto.builder().mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("1.1.1").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoFormatoTipoAccionError() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("accionNoExiste").mensaje("mensaje caducado").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("1.1.1").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }

    @Test
    public void ingresarVersion_debeRetornarException_cuandoMensajeMuyExtenso() {
        AccionDto accionDto = AccionDto.builder().tipoAccion("CADUCADA").mensaje("qazwsxedcrfvtgbyhnujmik,1234567899").build();
        VersionDto version = VersionDto.builder().plataforma("IOS").version("1.1.1").accion(accionDto).build();
        assertThatThrownBy(() ->  gestionVersionAppService.ingresarVersion(version)).isInstanceOf(ErrorNegocioException.class);
    }
}