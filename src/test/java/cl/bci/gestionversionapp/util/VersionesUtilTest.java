package cl.bci.gestionversionapp.util;

import cl.bci.gestionversionapp.model.AccionDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cl.bci.gestionversionapp.model.VersionDto;
import cl.bci.gestionversionapp.repository.AccionesRepository;
import cl.bci.gestionversionapp.repository.VersionesRepository;
import cl.bci.gestionversionapp.repository.entity.AccionesEntity;
import cl.bci.gestionversionapp.repository.entity.VersionesEntity;
import cl.bci.gestionversionapp.service.GestionVersionAppServiceImpl;
import cl.bci.mscore.exception.ErrorNegocioException;
import org.dozer.DozerBeanMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;


@RunWith(SpringRunner.class)
public class VersionesUtilTest {

    public String[] versiones;

    @InjectMocks
    private VersionesUtil versionesUtil;

    @Before
    public void setUp() throws Exception {
        List<VersionesEntity> responseRepo = new ArrayList<>();
        AccionesEntity accion = AccionesEntity.builder().tipoAccion("ACTUALIZAR").mensaje("mensaje ACTUALIZAR").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.1.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje ").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.1").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("ACTUALIZAR").mensaje("mensaje ACTUALIZAR").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("MANTENCION").mensaje("mensaje MANTENCION").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.3.4").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje ").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("VIGENTE").mensaje("mensaje VIGENTE").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("1.0.0").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("MANTENCION").mensaje("mensaje MANTENCION").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("2.1.1").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("mensaje CADUCADA").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("2").accionesEntity(accion).build());
        accion = AccionesEntity.builder().tipoAccion("CADUCADA").mensaje("mensaje CADUCADA").build();
        responseRepo.add(VersionesEntity.builder().plataforma("IOS").version("2.2.1").accionesEntity(accion).build());

        versiones = responseRepo.stream().map(e -> e.getVersion()).toArray(String[]::new);
    }

    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoEncuentraVersionYvalidaLArgos() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("2.0.1",versiones);
        assertThat(versionAnterior).isNotEmpty();
        assertThat(versionAnterior).isEqualTo("2");
    }

    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoEncuentraVersionPorPlataforma() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("1.4.1",versiones);
        assertThat(versionAnterior).isNotEmpty();
        assertThat(versionAnterior).isEqualTo("1.3.4");
    }


    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoEncuentraVersionYvalidaLArgosVersion() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("1.3",versiones);
        assertThat(versionAnterior).isNotEmpty();
        assertThat(versionAnterior).isEqualTo("1.2");
    }

    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoEncuentraVersionSuperiorAtExistentes() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("2.2.2",versiones);
        assertThat(versionAnterior).isNotEmpty();
        assertThat(versionAnterior).isEqualTo("2.2.1");
    }

    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoNoEncuentraVersionIgual() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("0.1.1",versiones);
        assertThat(versionAnterior).isEmpty();
        assertThat(versionAnterior).isEqualTo("");
    }

    @Test
    public void obtenerVersionAnterior_debeRetornarVigente_cuandoNoEncuentraVersionInferior() {
        String versionAnterior = versionesUtil.obtenerVersionAnterior("1.3.4",versiones);
        assertThat(versionAnterior).isNotEmpty();
        assertThat(versionAnterior).isEqualTo("1.3.4");
    }
}