package cl.bci.gestionversionapp.controller;

import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cl.bci.gestionversionapp.model.VersionDto;
import cl.bci.gestionversionapp.model.VersionesAppResponseDto;
import cl.bci.gestionversionapp.service.GestionVersionAppService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class GestionVersionAppControllerTest {

    @Mock
    private GestionVersionAppService service;

    @InjectMocks
    private GestionVersionAppController gestionVersionAppController;

    @Test
    public void validarVersionAppDebeRetornarDatosCuandoSeEncuentraLaPlataformaYVersion() {
        ValidarVersionAppRequestDto validarVersionAppRequestDto = new ValidarVersionAppRequestDto("ANDROID", "1.1.1");
        when(service.validarVersionApp(validarVersionAppRequestDto)).thenReturn(new ValidarVersionAppResponseDto("VIGENTE", "", "1.0.1"));

        ValidarVersionAppResponseDto validarVersionAppResponseDto = gestionVersionAppController.validarVersionApp(validarVersionAppRequestDto);
        assertThat(validarVersionAppResponseDto).isNotNull();
        assertThat(validarVersionAppResponseDto.getTipoAccion()).isNotEmpty();
        assertThat(validarVersionAppResponseDto.getMensaje()).isNotNull();
        assertThat(validarVersionAppResponseDto.getVersion()).isNotNull();
    }

    @Test
    public void solicitarVersiones() {
        List<VersionDto> versiones = Collections.singletonList(VersionDto.builder().build());
        when(service.solicitarVersiones()).thenReturn(versiones);
        VersionesAppResponseDto versionesAppResponseDto = gestionVersionAppController.solicitarVersiones();
        assertThat(versionesAppResponseDto).isNotNull();
        assertThat(versionesAppResponseDto.getVersiones()).isNotEmpty();
    }

    @Test
    public void ingresarVersion() {
        VersionDto versionDto = VersionDto.builder().build();
        when(service.ingresarVersion(versionDto)).thenReturn(versionDto);
        ResponseEntity responseEntity = gestionVersionAppController.ingresarVersion(versionDto);
        assertThat(responseEntity).isNotNull();
        assertThat(responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

    }
}