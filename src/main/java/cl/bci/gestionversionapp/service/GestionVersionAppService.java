package cl.bci.gestionversionapp.service;


import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cl.bci.gestionversionapp.model.VersionDto;

import java.util.List;

/**
 * 
 *  
 * 
 *@author: Celula Autenticacion.
 */
public interface GestionVersionAppService {

    ValidarVersionAppResponseDto validarVersionApp(ValidarVersionAppRequestDto validarVersionAppRequest);
    List<VersionDto> solicitarVersiones();
    VersionDto ingresarVersion(VersionDto versionDto);
}
