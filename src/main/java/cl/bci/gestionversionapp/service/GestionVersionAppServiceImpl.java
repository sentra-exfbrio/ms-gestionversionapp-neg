package cl.bci.gestionversionapp.service;

import cl.bci.gestionversionapp.model.*;
import cl.bci.gestionversionapp.repository.AccionesRepository;
import cl.bci.gestionversionapp.repository.VersionesRepository;
import cl.bci.gestionversionapp.repository.entity.AccionesEntity;
import cl.bci.gestionversionapp.repository.entity.VersionesEntity;
import cl.bci.gestionversionapp.util.VersionesUtil;
import cl.bci.mscore.exception.ErrorNegocioException;
import org.apache.commons.lang.StringUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.loader.api.BeanMappingBuilder;
import org.dozer.loader.api.FieldsMappingOptions;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import cl.bci.mscore.logger.Loggeable;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import java.util.stream.StreamSupport;

/** 
 * 
 *  
 * 
 *@author: Celula Autenticacion.
 */
@Loggeable
@Slf4j
@Service
public class GestionVersionAppServiceImpl implements GestionVersionAppService, InitializingBean {

	@Autowired
	private DozerBeanMapper dozerBeanMapper;

	@Autowired
	private VersionesUtil versionesUtil;

	@Autowired
	private VersionesRepository versionesRepository;

	@Autowired
    private AccionesRepository accionesRepository;

    @Value("#{'${validacion.plataformas}'.split(',')}")
    private List<String> plataformasPermitidas;

    @Value("${validacion.regexVersion}")
    private String regexVersion;

    @Value("#{'${validacion.tiposAcciones}'.split(',')}")
    private List<String> tiposAcciones;

    @Value("${validacion.largoMensaje}")
    private int largoMensaje;

    @Value("${accion.tipoDefault}")
    private String tipoAccionDefault;

    @Value("${accion.mensajeDefault}")
    private String mensajeDefault;

    private Pattern pattern;

    @PostConstruct
    public void init(){
        pattern = Pattern.compile(regexVersion);
    }

	@Override
	public void afterPropertiesSet() {
		BeanMappingBuilder builder = new BeanMappingBuilder() {
			protected void configure() {
				mapping(VersionesEntity.class, VersionDto.class)
						.fields("plataforma", "plataforma")
						.fields("version", "version")
						.fields("accionesEntity", "accion",
								FieldsMappingOptions.hintB(AccionDto.class));
			}
		};
		dozerBeanMapper.addMapping(builder);
	}

	public void setDozerBeanMapper(DozerBeanMapper dozerBeanMapper) {
		this.dozerBeanMapper = dozerBeanMapper;
	}

	public ValidarVersionAppResponseDto validarVersionApp(ValidarVersionAppRequestDto validarVersionAppRequest){
        validaVersionDto(VersionDto.builder()
                .plataforma(validarVersionAppRequest.getPlataforma())
                .version(validarVersionAppRequest.getVersion())
                .build()
        );
        validarVersionAppRequest.setPlataforma(validarVersionAppRequest.getPlataforma().toUpperCase()); //FIXME

        VersionesEntity versionesEntity = versionesRepository.findByPlataformaAndVersion(validarVersionAppRequest.getPlataforma(), validarVersionAppRequest.getVersion());

        if (versionesEntity !=  null){
            return dozerBeanMapper.map(versionesEntity.getAccionesEntity(), ValidarVersionAppResponseDto.class);
        }

        List<VersionesEntity> listadoVersiones = versionesRepository.findByPlataforma(validarVersionAppRequest.getPlataforma());

		if(listadoVersiones!= null && listadoVersiones.size() > 0){
			Map<String, VersionesEntity> map = listadoVersiones.stream().collect(Collectors.toMap(p -> p.getVersion(), p -> p));
            String[] versiones = listadoVersiones.stream().map(e -> e.getVersion()).toArray(String[]::new);
            String versionAnteriorStr = versionesUtil.obtenerVersionAnterior(validarVersionAppRequest.getVersion(), versiones);
            if (!StringUtils.isEmpty(versionAnteriorStr)) {
                VersionesEntity versionAnterior = map.get(versionAnteriorStr);
                return dozerBeanMapper.map(versionAnterior.getAccionesEntity(), ValidarVersionAppResponseDto.class);
            }
		}
        return ValidarVersionAppResponseDto.builder().tipoAccion(tipoAccionDefault).mensaje(mensajeDefault).version(validarVersionAppRequest.getVersion()).build();
	}

	public List<VersionDto> solicitarVersiones(){
		Iterable<VersionesEntity> all = versionesRepository.findAll();
        return StreamSupport.stream(all.spliterator(), false)
                .map(from -> this.dozerBeanMapper.map(from, VersionDto.class))
                .collect(Collectors.toList());
	}

    @Override
    /*
    plataforma solo puede ser IOS y ANDROID
    version debe ser en formato x.x.x
    Las acciones son VIGENTE, CADUCADO, ACTUALIZAR, MANTENCION
    los valores deben ir en el config server por si en un futuro agregan otra accion o blablaball
     */
    public VersionDto ingresarVersion(VersionDto versionDto) {
        validaVersionDto(versionDto);
        AccionDto accionDto = versionDto.getAccion();
        validaAccionDto(accionDto);

        VersionesEntity versionesEntity = versionesRepository.findByPlataformaAndVersion(versionDto.getPlataforma(), versionDto.getVersion());
        if (versionesEntity == null){
            versionesEntity = VersionesEntity.builder()
                    .plataforma(versionDto.getPlataforma())
                    .version(versionDto.getVersion())
                    .build();
        }

        if (versionesEntity.getAccionesEntity() != null && accionDto.getTipoAccion().equalsIgnoreCase(versionesEntity.getAccionesEntity().getTipoAccion())){
            versionesEntity.getAccionesEntity().setMensaje(accionDto.getMensaje());
        }
        else{
            AccionesEntity accionesEntity = accionesRepository.findByTipoAccion(accionDto.getTipoAccion());
            if (accionesEntity == null){
                accionesEntity = AccionesEntity.builder()
                                    .tipoAccion(accionDto.getTipoAccion())
                                    .mensaje(accionDto.getMensaje())
                                    .build();
            }
            accionesEntity.setMensaje(accionDto.getMensaje());
            versionesEntity.setAccionesEntity(accionesEntity);
        }
        versionesRepository.save(versionesEntity);
        return this.dozerBeanMapper.map(versionesEntity, VersionDto.class);
    }

    private void validaVersionDto(VersionDto versionDto) {
        if (versionDto == null){
            throw new ErrorNegocioException("EXGVA0001", "Debe ingresar body para la version");
        }
        if (StringUtils.isEmpty(versionDto.getPlataforma())){
            throw new ErrorNegocioException("EXGVA0002", "Debe ingresar una plataforma");
        }

        versionDto.setPlataforma(versionDto.getPlataforma().toUpperCase());

        if (!plataformasPermitidas.contains(versionDto.getPlataforma())){
            throw new ErrorNegocioException("EXGVA0003", "Formato plataforma invalido [" + versionDto.getPlataforma() + "]");
        }
        if (StringUtils.isEmpty(versionDto.getVersion())){
            throw new ErrorNegocioException("EXGVA0004", "Debe ingresar una version");
        }
        if (!pattern.matcher(versionDto.getVersion()).matches()){
            throw new ErrorNegocioException("EXGVA0005", "El formato de la version [" + versionDto.getVersion() + "] no es valido");
        }
    }

    private void validaAccionDto(AccionDto accionDto) {
        if (accionDto == null){
            throw new ErrorNegocioException("EXGVA0006", "Debe ingresar los datos de la accion");
        }
        if (StringUtils.isEmpty(accionDto.getTipoAccion())){
            throw new ErrorNegocioException("EXGVA0007", "Debe ingresar el tipo de accion");
        }
        accionDto.setTipoAccion(accionDto.getTipoAccion().toUpperCase());
        if (!tiposAcciones.contains(accionDto.getTipoAccion())){
            throw new ErrorNegocioException("EXGVA0008", "Fomato tipo accion invalido [" + accionDto.getTipoAccion() + "]");
        }

        if (StringUtils.isEmpty(accionDto.getMensaje())){
            accionDto.setMensaje("");
        }
        else if (accionDto.getMensaje().length() > largoMensaje){
            throw new ErrorNegocioException("EXGVA0009", "Largo del mensaje superior al permitido [" + largoMensaje + "] mensaje actual [" + accionDto.getMensaje().length() + "]");
        }
    }
}
