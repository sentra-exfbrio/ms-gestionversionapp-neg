package cl.bci.gestionversionapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableAsync;

/** 
 * 
 * Clase Main de proyecto GestionVersionApp 
 * 
 *@author: Celula Autenticacion.
 */
@SpringBootApplication 
@EnableEurekaClient
@EnableAsync
@Import(
	{
        cl.bci.mscore.base.filter.LoggerFilter.class,
        cl.bci.mscore.base.configuration.SwaggerConfiguration.class,
        cl.bci.mscore.base.security.ResourceServer.class,
        cl.bci.mscore.exception.ExceptionManager.class,
        cl.bci.mscore.logger.AspectLogger.class,
		cl.bci.mscore.base.security.config.RolesConfig.class
		//cl.bci.oauthtokenutil.core.configuration.BlackListTokenConfiguration.class
	}
)
public class GestionVersionAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestionVersionAppApplication.class, args);
    }
}
