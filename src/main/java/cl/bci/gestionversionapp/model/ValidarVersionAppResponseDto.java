package cl.bci.gestionversionapp.model;

import lombok.*;

/** 
 * 
 * Version de la aplicacion.
 * 
 *@author: Celula Autenticacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ValidarVersionAppResponseDto {

    private String tipoAccion;

    private String mensaje;

    private String version;



}
