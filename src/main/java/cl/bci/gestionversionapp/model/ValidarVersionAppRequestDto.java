package cl.bci.gestionversionapp.model;

import lombok.*;

/** 
 * 
 * Version de la aplicacion.
 * 
 *@author: Celula Autenticacion.
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ValidarVersionAppRequestDto {

    private String plataforma;

    private String version;


}
