package cl.bci.gestionversionapp.model;

import lombok.Builder;
import lombok.Data;

import java.util.List;

/** 
 * 
 * Version de la aplicacion.
 * 
 *@author: Celula Autenticacion.
 */
@Data
@Builder
public class VersionesAppResponseDto {

    private List<VersionDto> versiones; //NOSONAR

}
