package cl.bci.gestionversionapp.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/** 
 * 
 * Version de la aplicacion.
 * 
 *@author: Celula Autenticacion.
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class VersionDto {

    private String plataforma;

    private String version; //NOSONAR

    private AccionDto accion;

}
