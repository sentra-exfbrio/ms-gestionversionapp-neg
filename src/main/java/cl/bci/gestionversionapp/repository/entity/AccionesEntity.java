package cl.bci.gestionversionapp.repository.entity;

import lombok.*;

import javax.persistence.*;

/**
* Descipcion
* <p>
* <B>Todos los derechos reservados por Banco de Credito e Inversiones.</B>  
* <P>
*/
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "acciones",
        uniqueConstraints = @UniqueConstraint( columnNames= "tipoAccion" )
)
public class AccionesEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column(name = "tipoAccion")
    private String tipoAccion;

    @Column(name = "mensaje")
    private String mensaje;


}
