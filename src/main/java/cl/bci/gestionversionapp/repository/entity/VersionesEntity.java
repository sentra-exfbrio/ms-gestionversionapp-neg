package cl.bci.gestionversionapp.repository.entity;

import lombok.*;

import javax.persistence.*;

/**
* Descipcion
* <p>
* <B>Todos los derechos reservados por Banco de Credito e Inversiones.</B>  
* <P>
*/
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "versiones",
        uniqueConstraints = @UniqueConstraint( columnNames= {"plataforma","version"} )
    )
public class VersionesEntity {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id;

    @Column(name = "plataforma")
    private String plataforma;

    @Column(name = "version")
    private String version;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_accion", referencedColumnName = "id")
    private AccionesEntity accionesEntity;
}
