package cl.bci.gestionversionapp.repository;

import cl.bci.gestionversionapp.repository.entity.AccionesEntity;
import cl.bci.gestionversionapp.repository.entity.VersionesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by SEnTRA on 07-06-2018.
 */
@Repository
public interface AccionesRepository extends CrudRepository<AccionesEntity, Integer> {

    AccionesEntity findByTipoAccion(String accion);

}
