package cl.bci.gestionversionapp.repository;

import cl.bci.gestionversionapp.repository.entity.VersionesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by SEnTRA on 07-06-2018.
 */
@Repository
public interface VersionesRepository extends CrudRepository<VersionesEntity, Integer> {

    public VersionesEntity findByPlataformaAndVersion(String plataforma, String version);
    public List<VersionesEntity> findByPlataforma(String plataforma);

}
