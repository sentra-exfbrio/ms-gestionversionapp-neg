package cl.bci.gestionversionapp.controller;

import cl.bci.gestionversionapp.model.ValidarVersionAppRequestDto;
import cl.bci.gestionversionapp.model.ValidarVersionAppResponseDto;
import cl.bci.gestionversionapp.model.VersionDto;
import cl.bci.gestionversionapp.model.VersionesAppResponseDto;
import cl.bci.gestionversionapp.service.GestionVersionAppService;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/** 
 * 
 * Clase controlador de endpoints de Microservicio 
 * 
 *@author: Celula Autenticacion.
 */
@Slf4j
@RestController
@RequestMapping("/Transversales/Utilitarios/ms-gestionVersionApp-neg-V1.0")
public class GestionVersionAppController{

	@Autowired
    private GestionVersionAppService service;


	/**
	 * Obtiene un tipo de accion y mensaje segun la plataforma y version de la app enviados.
	 * 
	 * @author Celula Autenticacion.
	 * @param versionAppRequest
	 * @return ValidarVersionAppResponseDto
	 */
    @ApiOperation(
		"Operacion encargada de validar la version de la aplicacion del usuario [ValidarVersionApp]"
	)
    @PostMapping(
		value = "/validarVersionApp",
		produces = MediaType.APPLICATION_JSON_VALUE
	)
	@PreAuthorize("hasAnyRole(@rolesConfig.getRolesPublico(),@rolesConfig.getRoles('sistema'))")
    public ValidarVersionAppResponseDto validarVersionApp(@RequestBody ValidarVersionAppRequestDto versionAppRequest) {
    	return service.validarVersionApp(versionAppRequest);
		
    }

	/**
	 * Lista las versiones y sus acciones.
	 *
	 * @author Autenticacion
	 * @return VersionesAppResponseDto
	 */
	@ApiOperation(
			"Operacion encargada de listar las versiones de la App registradas"
	)
	@GetMapping(
			value = "/solicitarVersiones",
			produces = MediaType.APPLICATION_JSON_VALUE
	)
    @PreAuthorize("hasAnyRole(@rolesConfig.getRoles('sistema'))")
	public VersionesAppResponseDto solicitarVersiones() {
        return VersionesAppResponseDto.builder().versiones(service.solicitarVersiones()).build();
	}

    /**
     * Inserta o actualiza una version con accion.
     *
     * @author Celula Autenticacion
     * @param versionDto
     * @return ResponseEntity
     */
    @ApiOperation(
            "Operacion encargada de ingresar o actualizar las versiones y mensajes"
    )
    @PostMapping(
            value = "/configurarVersion", //FIXME
            produces = MediaType.APPLICATION_JSON_VALUE
    )
	@PreAuthorize("hasAnyRole(@rolesConfig.getRoles('sistema'))")
    public ResponseEntity ingresarVersion(@RequestBody VersionDto versionDto) {
		return ResponseEntity.ok(service.ingresarVersion(versionDto));
    }
}
