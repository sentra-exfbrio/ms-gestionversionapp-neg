package cl.bci.gestionversionapp.configuration;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

import javax.sql.DataSource;

/**
* Descipcion
* <p>
* <B>Todos los derechos reservados por Banco de Credito e Inversiones.</B>  
* <P>
*/
@Configuration
@EnableJpaRepositories(basePackages = "cl.bci.gestionversionapp.repository")
public class VersionesAppDataSourceConfiguration {

    @ConfigurationProperties(prefix = "db-postgres-autenticacion-versionapp")
    @Bean
    @Primary
    public DataSource postgresDataSource() {
        return DataSourceBuilder.create().
                build();
    }

    @Bean(name = "entityManagerFactory")
    @Primary
    public LocalContainerEntityManagerFactoryBean emf1(EntityManagerFactoryBuilder builder){
        return builder
                .dataSource(postgresDataSource())
                .packages("cl.bci.gestionversionapp.repository.entity")
                .persistenceUnit("users")
                .build();
    }

}
