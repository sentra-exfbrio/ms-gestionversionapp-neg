package cl.bci.gestionversionapp.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import cl.bci.mscore.base.client.RestTemplateMs;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;

/** 
 * 
 * Configuracion de cliente Rest 
 * 
 *@author: Celula Autenticacion.
 */
@Configuration
public class RestClientConfiguration {

	@Value("${spring.application.name}")
	private String referenceService;
	
	//Para llamada de servicios rest (no microservicios)
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
	
	//RestTemplate para llamadas entre microservicios
	
	@LoadBalanced
	@Bean
	public RestTemplateMs restTemplateMs() {
       return new RestTemplateMs(this.referenceService);
   }
}


