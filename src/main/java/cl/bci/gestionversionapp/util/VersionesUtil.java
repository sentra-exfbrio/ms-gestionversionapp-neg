package cl.bci.gestionversionapp.util;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Comparator;

@Component
public class VersionesUtil {
    
    public String obtenerVersionAnterior(String version, String[] versiones){
        Arrays.sort(versiones, new VersionNumberComparator());
        if(compareTo(versiones[0], version)> 0){
            return "";
        }
        for(int i=1; i< versiones.length-1;i++){
            int compare = compareTo(versiones[i], version);
            if (compare > 0){
                return versiones[i-1];
            }
        }
        return versiones[versiones.length-1];
    }

    public int compareTo(String primeraVersion, String segundaVersion) {//-1 s1 menor s2 //1  s1 mayor s2 //0  s1 igual s2
        String[] partesPraVersion = primeraVersion.split("\\.");
        String[] partesSdaVersion = segundaVersion.split("\\.");
        int maximoLargo = Math.max(partesPraVersion.length, partesSdaVersion.length);
        for(int i = 0; i < maximoLargo; i++) {
            int thisPart = i < partesPraVersion.length ?
                Integer.parseInt(partesPraVersion[i]) : 0;
            int thatPart = i < partesSdaVersion.length ?
                Integer.parseInt(partesSdaVersion[i]) : 0;
            if(thisPart < thatPart)
                return -1;
            if(thisPart > thatPart)
                return 1;
        }
        return 0;
    }
    
    public final static class VersionNumberComparator implements Comparator<String> {
        public int compare(String primeraVersion, String segundaVersion) {
          String[] partesPraVersion = primeraVersion.split("\\.");
          String[] partesSdaVersion = segundaVersion.split("\\.");
          int major1 = major(partesPraVersion);
          int major2 = major(partesSdaVersion);
          if (major1 == major2) {
            return minor(partesPraVersion).compareTo(minor(partesSdaVersion));
          }
          return major1 > major2 ? 1 : -1;
        }
        private int major(String[] version) {
          return Integer.parseInt(version[0]);
        }
        private Integer minor(String[] version) {
          return version.length > 1 ? Integer.parseInt(version[1]) : 0;
        }
      }
    
}
   
   
    
 
